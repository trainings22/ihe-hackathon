# IHE hackathon

Some tiny ansible files to get started with SURF Research Cloud catalog creation.

## Ansible

- https://docs.ansible.com/ansible/latest/


## yaml

- https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html
- https://yaml-multiline.info/
- https://www.w3schools.io/file/yaml-introduction/


## Call Ansible Playbook in terminal

ansible-playbook my-playbook.yml [--check, --extra-vars name=value ]

